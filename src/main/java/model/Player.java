package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Player {

    private String name;
    private List<Card> cards;

    public Player(String name) {
        this.name = name;
    }
}
