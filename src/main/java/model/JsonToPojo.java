package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JsonToPojo {
    Player playerA;
    Player playerB;
    Boolean playerAWins;
}
