import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Card;
import model.Player;
import model.JsonToPojo;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class YocoJack {

    public static void main(String[] args) throws IOException {

        for (JsonToPojo jsonToPojo : convertJsonToPojo()) {
            playerAWins(jsonToPojo.getPlayerA(), jsonToPojo.getPlayerB());
        }
    }

    private static Boolean playerAWins(Player playerA, Player playerB) {
        boolean playerAWins = false;
        Integer playerAScore = 0;
        Integer playerBScore = 0;
        playerA.getCards().sort(Comparator.comparing(Card::getValue));
        playerB.getCards().sort(Comparator.comparing(Card::getValue));
        for (Card card : playerA.getCards()) {
            playerAScore += card.getValue();
            if (playerAScore > 21) {
                return false;
            }
        }
        for (Card card : playerB.getCards()) {
            playerBScore += card.getValue();
            if (playerBScore > 21) {
                return true;
            }
        }
        if (playerAScore > playerBScore) {
            return true;
        }
        /*
         * set index to shortest list size - 1
         * compare last item if value greate player a wins else check if equal
         * if equal check second last item (index value -1)
         * else false
         * */

        if (playerAScore.equals(playerBScore)) {
            int playerAIndex = playerA.getCards().size() - 1;
            int playerBIndex = playerB.getCards().size() - 1;
            int index = playerA.getCards().size() < playerB.getCards().size() ? playerA.getCards().size() - 1 : playerB.getCards().size() - 1;
            Integer i =  playerA.getCards().size() < playerB.getCards().size() ? playerA.getCards().size() - 1 : playerB.getCards().size() - 1;
            return compare(playerA.getCards(), playerB.getCards(), i);
//            while (index <= playerAIndex && index <= playerBIndex) {
//                if (playerA.getCards().get(index).getValue() > playerB.getCards().get(index).getValue()) {
//                    return true;
//                }
//                if (playerB.getCards().get(index).getValue() > playerA.getCards().get(index).getValue()) {
//                    return false;
//                }
//
//                index += 1;
//                if (playerA.getCards().get(index).getValue().equals(playerB.getCards().get(index).getValue())) {
//                    if (playerA.getCards().get(index + 1).getValue() > playerB.getCards().get(index + 1).getValue()) {
//                        return true;
//                    }
//                }
//            }
        }
        return playerAWins;
    }

    private static List<JsonToPojo> convertJsonToPojo() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode arrNode = objectMapper.readTree(new File("src/main/resources/tests.json"));
        List<JsonToPojo> jsonToPojos = new ArrayList<>();
        for (JsonNode jsonNode : arrNode) {
            Player playerA = new Player("playerA", new ArrayList<>());
            Player playerB = new Player("playerB", new ArrayList<>());
            ArrayList playerAArray = new ObjectMapper().convertValue(jsonNode.get("playerA"), ArrayList.class);
            ArrayList playerBArray = new ObjectMapper().convertValue(jsonNode.get("playerB"), ArrayList.class);

            String[] JQK = {"A", "J", "Q", "K"};
            for (Object value : playerAArray) {
                String playerA_hand = (String) value;
                String playerA_cardStringValue = playerA_hand.substring(0, playerA_hand.length() - 1);
                String playerA_suit = playerA_hand.substring(playerA_hand.length() - 1);
                Integer playerA_cardValue = Arrays.asList(JQK).contains(playerA_cardStringValue) ? 10 : Integer.parseInt(playerA_cardStringValue);
                Card playerA_card = new Card(playerA_cardValue, playerA_suit);
                playerA.getCards().add(playerA_card);
            }
            for (Object o : playerBArray) {
                String playerB_hand = (String) o;
                String playerB_cardStringValue = playerB_hand.substring(0, playerB_hand.length() - 1);
                String playerB_suit = playerB_hand.substring(playerB_hand.length() - 1);
                Integer playerB_cardValue = Arrays.asList(JQK).contains(playerB_cardStringValue) ? 10 : Integer.parseInt(playerB_cardStringValue);
                Card playerB_card = new Card(playerB_cardValue, playerB_suit);
                playerB.getCards().add(playerB_card);
            }
            Boolean playerAWins = new ObjectMapper().convertValue(jsonNode.get("playerAWins"), Boolean.class);
            jsonToPojos.add(new JsonToPojo(playerA, playerB, playerAWins));
        }

        return jsonToPojos;
    }

    public static Boolean compare(List<Card> playerACards, List<Card> playerBCards, Integer index) {
//        playerACards.size() < playerBCards.size() ? playerACards.size() - 1 : playerBCards.size() - 1;

        if (playerACards.get(index).getValue() > playerBCards.get(index).getValue()) {
            return true;
        } else if (playerACards.get(index).getValue().equals(playerBCards.get(index).getValue())) {
            compare(playerACards, playerBCards, index +1);

        }
            return false;

    }
    /*
     * set index to shortest list size - 1
     * compare last item if value greate player a wins else check if equal
     * if equal check second last item (index value -1)
     * else false
     * */
}
